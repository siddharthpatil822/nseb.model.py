from django.db import models
#from django.forms import MultipleChoiceField,  Form
from datetime import  datetime,date

from django.db.models.options import Options

class Stream(models.Model):
    stream_id = models.AutoField(primary_key=True)
    stream_name = models.CharField(max_length=200)
    pub_date =models.DateField(auto_now_add=False,auto_now=False,blank=True,null=True)
    #timestamp =models.DateField(auto_now_add=True,auto_now=False,blank=True,null=True)
    def __str__(self): 
    	return self.stream_name


class Subject(models.Model):
   subject_name = models.CharField(max_length=200)
   #subject_id = models.AutoField(primary_key=False,null=False)
   stream_id  = models.ForeignKey(Stream, on_delete=models.CASCADE)
   def __str__(self): 
    	return self.subject_name
   # votes = models.IntegerField(default=0)


class Question_bank(models.Model):
   qb_id =models.AutoField(primary_key=True)
   name = models.CharField(max_length=200)
   stream = models.ForeignKey(Stream, on_delete=models.CASCADE)
   subject_id  = models.ForeignKey(Subject, on_delete=models.CASCADE)
   pub_date = models.DateTimeField('date published')
   def __str__(self): 
    	return self.name

  


               
     
class Complexity(models.Model):
    #complexity_id =  models.AutoField(primary_key=True)
    #complexity_name = models.CharField(max_length=20)
    Complexity_CHOICES=(('simple','SIMPLE'),
    ('medium', 'MEDIUM'),
    ('hard','HARD'),
    )
    
    
    complexity_name = models.CharField(max_length=6,choices=Complexity_CHOICES)
    def __str__(self): 
    	return self.complexity_name
class Question(models.Model):
   question_id =models.AutoField(primary_key=True)
   qb_id = models.ForeignKey(Question_bank, on_delete=models.CASCADE)
   question = models.TextField(max_length=200,)
   options = (
    ('DELHI', "Delhi"),
    ('MUMBAI', "Mumbai"),
    ('PUNA', "Puna"),
    ('BENGALORE', "Bengalore"),
)

options =models. CharField(max_length=9,
                  choices=Options)
correct_answer=models.TextField(max_length=200,)
option1 = models.TextField(max_length=200)
option2 = models.TextField(max_length=200)
option3 = models.TextField(max_length=200)
option4 = models.TextField(max_length=200)
fields = ['question','option1','option2','option3','option4'] 
   #complex=models.ForeignKey(Complexity,on_delete=models.CASCADE)
def __str__(self): 
    	return self.question 

 
    
class Exam_creation(models.Model):

    exam_name = models.CharField(max_length=200)
    start_date = models.DateTimeField('start date ')
    end_date = models.DateTimeField('end date ')
    entry_fee = models.IntegerField(default=0)
    practice=models.BooleanField(default=True)
    prize = models.IntegerField(default=0)
    complexity = models.ForeignKey(Complexity, on_delete=models.CASCADE)
    stream = models.ForeignKey(Stream, on_delete=models.CASCADE)
    question_bank = models.ForeignKey(Question_bank, on_delete=models.CASCADE)
    no_of_question = models.IntegerField(default=0)
    #syallbus = models.TextField(max_length=200)
    #Practice_exams = models.TextField(max_length=200)
    Clause  = models.TextField(max_length=200)
    fields = ['exam_name','start_date','end_date','entry_fee','prize','no_of_question','practice_exams']
    def __str__(self): 
    	return self.exam_name
    #def clean(self):
     #   cleaned_data = super(Exam_creation, self).clean()
      #  from_time = cleaned_data.get("from_time")
       # end_time = cleaned_data.get("end_time")

        #if from_time and end_time:
         #   if end_time < from_time:
          #      raise forms.ValidationError("End time cannot be earlier than start time!")
        #return cleaned_data
                

class Syallbus_name(models.Model):

    subject_id =  models.ForeignKey(Subject, on_delete=models.CASCADE)
    syallbus = models.TextField(max_length=200)
    def __str__(self): 
    	return self.syallbus

 